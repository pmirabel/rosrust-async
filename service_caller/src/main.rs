use std::{thread, time::Duration};

use env_logger;
use rosrust;
use std::time::Duration;

#[tokio::main]
async fn main() -> () {
    env_logger::init();
    rosrust::init("service_caller");
    rosrust::wait_for_service("service_server", Some(time::Duration::from_secs(15))).unwrap();
    let client = rosrust::client::<rosrust_msg::std_srvs::Trigger>("service_server").unwrap();

    let ros_task = tokio::spawn(async {
        let rate = rosrust::rate(0.5);
        while rosrust::is_ok() {
            rate.sleep();
            rosrust::ros_info!!("Rosrust Heartbeat");
        }
    });

    let call_service_task = tokio::task::spawn_blocking(move || {
        // Asynchronous call that can be resolved later on
        let retval = client.req_async(rosrust_msg::std_srvs::Trigger {}});
        rosrust::ros_info!("{} + {} = {}", a, b, retval.read().unwrap().unwrap().sum);
    });


    rosrust::ros_info!!("Starting ros task");
    let result = a.await.unwrap();

    rosrust::ros_info!!("Hello World, {}", result);
    rosrust::ros_info!!("Hello World, {}", result);
}
