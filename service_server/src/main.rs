use std::{thread, time::Duration};

use env_logger;
use rosrust;

fn main() {
    env_logger::init();

    // Initialize node
    rosrust::init("service_server");

    // Create subscriber
    // The subscriber is stopped when the returned object is destroyed
    // let subscriber_info = rosrust::subscribe("chatter", 2, |v: rosrust_msg::std_msgs::String| {
    //     // Callback for handling received messages
    //     rosrust::ros_info!("Received: {}", v.data);
    // })
    // .unwrap();

    // Create service
    // The service is stopped when the returned object is destroyed
    let _service_raii =
        rosrust::service::<rosrust_msg::std_srvs::TriggerReq, _>("service", move |req| {
            thread::sleep(Duration::from_millis(1000 * 10));
            Ok(rosrust_msg::std_srvs::TriggerRes {
                success: true,
                message: String::from("COUCOU_PILOU_APRES_10_SECONDES"),
            })
        })
        .unwrap();

    let rate = rosrust::rate(30.0);
    while rosrust::is_ok() {
        rate.sleep();
    }
}
